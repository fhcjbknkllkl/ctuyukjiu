#!/bin/bash

# Global variables
DIR_CONFIG="/etc/v2ray"
DIR_RUNTIME="/user/bin"
DIR_TMP="$(mktemp -d)"

UUID=41934982-adc2-44ca-be02-c51802b138dc
WSPATH=/vless
PORT=80

# Write V2Ray configuration
cat << EOF > ${DIR_TMP}/heroku.json
{
  "inbounds": [
   {
     "port": ${PORT},
     "protocol": "vless",
     "settings": {
       "clients": [
         {
           "id": "$UUID"
         }
       ],
        "decryption":"none"
     },
     "streamSettings": {
       "network": "ws",
       "wsSettings": {
       "path": "/$WSPATH"
       }
     }
   }
 ],
 "outbounds": [
   {
     "protocol": "freedom",
     "settings": {}
   }
 ]
}

EOF

# 运行 xray
cat >/etc/systemd/system/web.service<<-EOF
[Unit]
Description=web Service
Documentation=https://github.com/xtls
After=network.target nss-lookup.target

[Service]
User=root
#User=nobody
#CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
NoNewPrivileges=true
ExecStart=/root/web run -c /root/config.json
Restart=on-failure
RestartPreventExitStatus=23

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable web.service
systemctl start web.service

# 显示节点信息
sleep 15
ARGO=$(cat argo.log | grep -oE "https://.*[a-z]+cloudflare.com" | sed "s#https://##")
cat > list << EOF

vless://${UUID}@www.digitalocean.com:443?encryption=none&security=tls&type=ws&host=${ARGO}&path=/${WSPATH}&sni=${ARGO}#Argo-Vless
===============================================================================
- {name: Argo-Vless, type: vless, server: www.digitalocean.com, port: 443, uuid: ${UUID}, tls: true, servername: ${ARGO}, skip-cert-verify: false, network: ws, ws-opts: {path: /${WSPATH}, headers: { Host: ${ARGO}}}, udp: true}
EOF

echo -e "\n↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n"
cat list
echo -e "\n 节点保存在文件: /root/list \n"
echo -e "\n↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n"

